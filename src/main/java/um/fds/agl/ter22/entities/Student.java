package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Student extends UserTER {

    private @ManyToOne GroupTER group;

    public Student(String firstName, String lastName) {
        super(firstName, lastName);
        String[] roles = { "ROLE_STUDENT" };
        this.setRoles(roles);
    }

    public Student(long id, String firstName, String lastName) {
        super(id, firstName, lastName);
        String[] roles = { "ROLE_STUDENT" };
        this.setRoles(roles);
    }

    public Student(String firstName, String lastName, GroupTER group) {
        super(firstName, lastName);
        String[] roles = { "ROLE_STUDENT" };
        this.setRoles(roles);
        this.group = group;
    }

    public Student(long id, String firstName, String lastName, GroupTER group) {
        super(id, firstName, lastName);
        String[] roles = { "ROLE_STUDENT" };
        this.setRoles(roles);
        this.group = group;
    }

    public Student() {
    }

    public void setGroup(GroupTER group) {
        this.group = group;
    }

    public GroupTER getGroup() {
        return this.group;
    }

}
