package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class TERProject {
	private String title;
	private @ManyToOne Teacher teacher;
	private @ManyToOne Student student;
	private @ManyToOne TERManager terManager;
	private @Id @GeneratedValue Long id;

	public TERProject() {
	}

	public TERProject(String title, Teacher teacher, Student student, TERManager manager) {
		this.title = title;
		this.teacher = teacher;
		this.terManager = manager;
		this.student = student;
	}

	public TERManager getTerManager() {
		return terManager;
	}

	public void setTerManager(TERManager terManager) {
		this.terManager = terManager;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Project{" +
				"id=" + getId() +
				", Title='" + getTitle() + '\'' +
				", Teacher='" + getTeacher() + '\'' +
				", manager='" + getTerManager() + '\'' +
				'}';
	}
}
