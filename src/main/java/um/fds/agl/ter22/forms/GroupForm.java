package um.fds.agl.ter22.forms;

public class GroupForm {
    private long id;
    private String groupName;

    public GroupForm(long id, String groupName) {
        this.groupName = groupName;
        this.id = id;
    }

    public GroupForm() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
