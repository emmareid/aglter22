package um.fds.agl.ter22.repositories;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import um.fds.agl.ter22.entities.TERManager;
import um.fds.agl.ter22.entities.Teacher;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class TeacherRepositoryTest {
    @Autowired
    private TeacherRepository teachers;
    @Autowired
    private TERManagerRepository managers;

    @Test
    void saveIsPossibleForManager() {
        SecurityContextHolder.getContext()
                .setAuthentication((Authentication) new UsernamePasswordAuthenticationToken("lechef",
                        "peuimporte", AuthorityUtils.createAuthorityList("ROLE_MANAGER")));

        TERManager terM1Manager = new TERManager("Mathieu", "lechef", "mdp", "ROLE_MANAGER");

        this.managers.save(terM1Manager);
        this.teachers.save(new Teacher("Margaret", "Hamilton", "margaret",
                terM1Manager, "ROLE_TEACHER"));
        assertNotNull(teachers.findByLastName("Hamilton"));
    }

    @Test
    void saveIsNotPossibleForTeacher() {
        SecurityContextHolder.getContext()
                .setAuthentication((Authentication) new UsernamePasswordAuthenticationToken("leprof",
                        "peuimporte", AuthorityUtils.createAuthorityList("ROLE_TEACHER")));

        try {
            this.teachers.save(new Teacher("Margaret", "Hamilton", "margaret",
                    new TERManager("Mathieu", "lechef", "mdp", "ROLE_TEACHER"), "ROLE_TEACHER"));
            fail();
        } catch (Exception e) {
        }
    }
}
